import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  counter:number;
  gameStarted: boolean = false;

  odds:Array<number> = [];
  evens:Array<number> = [];

  ngOnInit() {}

  gameStartedChange(event) {
    this.gameStarted = event.isStarted;
    console.log(
      `Game Status changed to: ${this.gameStarted} counter: ${event.counter}`
    );
  }

  counterChange(count) {
    this.counter = count;

    let arr = count % 2 === 0 ? this.evens : this.odds;
    arr.push(count);

    console.log(`counter from app component ${this.counter}`);
  }
}
