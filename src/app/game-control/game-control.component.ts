import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  OnDestroy,
  Output
} from "@angular/core";

@Component({
  selector: "app-game-control",
  templateUrl: "./game-control.component.html",
  styleUrls: ["./game-control.component.css"]
})
export class GameControlComponent implements OnInit {
  STOPPED: string = `Stopped`;
  STARTED: string = `Started`;

  interval: number;
  counter: number = 0;

  @Input()
  gameStarted: boolean;
  @Output()
  gameStartedChange: EventEmitter<{
    isStarted: boolean;
    counter: number;
  }> = new EventEmitter();

  @Output()
  counterChange: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  ngOnDestroy() {
    console.warn(`destroying`);
    clearInterval(this.interval);
  }

  getStatus() {
    return this.gameStarted ? this.STARTED : this.STOPPED;
  }

  handleStartGame() {
    console.log(`Starting game`);

    this.interval = setInterval(() => {
      this.counter++;
      this.counterChange.emit(this.counter);
    }, 1000);

    this.gameStartedChange.emit({ isStarted: true, counter: this.counter });
  }

  handleStopGame() {
    console.log(`Stopping game`);

    clearInterval(this.interval);

    this.gameStartedChange.emit({ isStarted: false, counter: this.counter });
  }
}
